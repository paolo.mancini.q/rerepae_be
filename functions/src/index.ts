import * as admin from "firebase-admin";
import * as bodyParser from "body-parser";
import * as cors from "cors";
import * as express from "express";
import * as functions from "firebase-functions";

import crudConfig from "./routes/crud/config";
import userConfig from "./routes/users/config";

// // Start writing Firebase Functions
// // https://firebase.google.com/docs/functions/typescript
admin.initializeApp();
const app = express();
app.use(bodyParser.json());
app.use(cors({ origin: true }));

userConfig(app);
crudConfig(app, "instituciones", [
  "tipo",
  "codigo",
  "abreviacion",
  "ruc",
  "nombre",
  "creadorId",
  "creadorNombre",
  "publico",
]);

crudConfig(app, "ods", ["codigo", "descripcion", "meta", "nombre"]);
crudConfig(app, "ods_meta", ["codigo", "descripcion", "meta", "nombre"]);
crudConfig(app, "ods_objetivos", ["codigo", "descripcion", "meta", "nombre"]);
crudConfig(app, "delineamientos", [
  "codigo",
  "descripcion",
  "nombre",
  "ods",
  "tipo",
  "institucion",
  "creadorId",
  "creadorNombre",
  "publico",
]);

// propietario se carga automaticamente del request
crudConfig(app, "evaluaciones", [
  "titulo",
  "activo",
  "estado",
  "indicadores",
  "lineamiento",
  "marco",
  "referencias",
  "resumen",
]);

// propietario
crudConfig(app, "indicadores", [
  "activo",
  "codigo",
  "nombre",
  "descripcion",
  "formula",
  "frecuencia",
  "institucion",
  "ods",
  "referencias",
  "tendencias",
  "unidadMedida",
  "creadorId",
  "creadorNombre",
  "publico",
]);

crudConfig(app, "perfiles", [
  "idUsuario",
  "institucion",
  "nombre",
  "roles",
  "telefono",
]);

crudConfig(app, "sexo", ["nombre", "sigla"]);

crudConfig(app, "region", ["nombre", "sigla"]);

crudConfig(app, "rango_etario", ["nivel", "nombre", "padre"]);

crudConfig(app, "tipo_institucion", ["descripcion", "siglas"]);

crudConfig(app, "tipo_lineamiento", ["descripcion", "siglas", "tipo"]);

export const api = functions.https.onRequest(app);
