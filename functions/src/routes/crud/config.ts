import { Application } from "express";
import crud from "./controller";
import { isAuthenticated } from "../../auth/authenticated";

export default function (
  app: Application,
  collection: string,
  fields: Array<string>
) {
  const { create, get, patch, all, remove } = crud(collection, fields);
  app.use(`/${collection}`, isAuthenticated);

  app.post(`/${collection}`, create);
  app.get(`/${collection}`, [all]);
  app.get(`/${collection}/:id`, [get]);
  app.put(`/${collection}/:id`, [patch]);
  app.delete(`/${collection}/:id`, [remove]);
}
