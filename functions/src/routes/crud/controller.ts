import * as HttpStatus from "http-status-codes";
import * as admin from "firebase-admin";
import * as functions from "firebase-functions";

import { Request, Response } from "express";

// collectionRef.add({foo: 'bar'}).then(documentReference => {
//   let firestore = documentReference.firestore;
//   console.log(`Root location for document is ${firestore.formattedName}`);
// });

export default function Crud(collection: string, fields: Array<string>) {
  const firestore = admin.firestore();
  const collectionRef = firestore.collection(collection);

  async function create(req: Request, res: Response) {
    try {
      const propiedades = Object.keys(req.body);
      const tieneTodo = fields.reduce(
        (tiene, campo) => tiene && propiedades.includes(campo),
        true
      );

      if (!tieneTodo) {
        return res.status(HttpStatus.BAD_REQUEST).json({
          success: false,
          message: `Falta campos, debe contener { ${fields.join(", ")}}`,
        });
      }

      const obj = fields.reduce(
        (acc, clave) => Object.assign(acc, { [clave]: req.body[clave] }),
        {}
      );

      const data = await collectionRef.add(obj);

      return res
        .status(HttpStatus.CREATED)
        .json({ success: true, data: data.id });
    } catch (err) {
      return handleError(res, err);
    }
  }

  function all(req: Request, res: Response) {
    
    collectionRef
      .get()
      .then((snapshot) => {
        const data: any[] = [];
        snapshot.forEach((doc) => data.push({ id: doc.id, ...doc.data() }));

        return res.status(HttpStatus.OK).json({ success: true, data });
      })
      .catch((err) => {
        return handleError(res, err);
      });
  }

  async function get(req: Request, res: Response) {
    const { id } = req.params;
    functions.logger.log(res.locals);

    try {
      const data = await collectionRef.doc(id).get();

      return res
        .status(HttpStatus.OK)
        .json({ success: true, data: { id: data.id, ...data.data() } });
    } catch (err) {
      return handleError(res, err);
    }
  }

  async function patch(req: Request, res: Response) {
    try {
      const { id } = req.params;
      const propiedades = Object.keys(req.body);

      const tieneTodo = fields.reduce(
        (tiene, campo) => tiene && propiedades.includes(campo),
        true
      );

      if (!tieneTodo) {
        return res.status(HttpStatus.BAD_REQUEST).json({
          success: false,
          message: `Falta campos, debe contener { ${fields.join(", ")}}`,
        });
      }

      const obj = fields.reduce(
        (acc, clave) => Object.assign(acc, { [clave]: req.body[clave] }),
        {}
      );

      await collectionRef.doc(id).set(obj);

      return res.status(HttpStatus.OK).json({ success: true, data: id });
    } catch (err) {
      return handleError(res, err);
    }
  }

  async function remove(req: Request, res: Response) {
    const { id } = req.params;

    try {
      await collectionRef.doc(id).delete();
      return res.status(HttpStatus.OK).json({ success: true });
    } catch (err) {
      return handleError(res, err);
    }
  }

  // TODO: factor out to middleware
  function handleError(res: Response, err: any) {
    return res
      .status(HttpStatus.INTERNAL_SERVER_ERROR)
      .json({ success: false, message: `${err.code} - ${err.message}` });
  }

  return { create, all, get, patch, remove };
}
